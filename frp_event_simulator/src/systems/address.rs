// use bevy::prelude::*;
// use bevy_turborand::prelude::*;
// use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};
// use uuid::Builder as UuidBuilder;

// use crate::{
//     components::*,
//     event::{address_registration_event, ADDRESS_TOPIC},
// };

// #[allow(clippy::type_complexity)]
// pub fn sim_address_registration(
//     mut commands: Commands,
//     mut query: Query<(Entity, &mut RngComponent, &Id, &Parent), (With<Person>, Without<Address>)>,
//     p_query: Query<&Address>,
//     clock_state: Res<ClockState>,
//     nats_state: Res<NatsState>,
// ) {
//     for (entity, mut rng, id, parent) in query.iter_mut() {
//         let now = clock_state.get_time();

//         if !Address::event_occurs(&mut rng, &clock_state) {
//             continue;
//         }

//         let parent_id = parent.get();

//         let parent_address = p_query.get_component::<Address>(parent_id);
//         let address_id = parent_address.map(|p| p.id).unwrap_or_else(|_| {
//             let mut bytes = [0; 16];
//             rng.fill_bytes(&mut bytes);
//             UuidBuilder::from_bytes(bytes).into_uuid()
//         });

//         debug!(
//             time = now.to_string(),
//             eid = entity.index(),
//             aid = address_id.to_string(),
//             "ADDRESS "
//         );

//         let entity_cmd = commands.get_entity(entity);

//         if let Some(mut entity_cmd) = entity_cmd {
//             entity_cmd.insert(Address::new(&address_id.to_string()));

//             let message = address_registration_event(&mut rng, &now, id.uuid, address_id);

//             nats_state
//                 .connection
//                 .publish(ADDRESS_TOPIC, message.to_string())
//                 .unwrap();
//         }
//     }
// }
