use crate::{
    components::*,
    event::{name_change_event, NAME_TOPIC},
};
use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

#[allow(clippy::type_complexity)]
pub fn sim_data_error_correction(
    mut commands: Commands,
    mut query: Query<
        (Entity, &mut RngComponent, &Id, &PersonName, &DataError),
        (With<Person>, With<DataError>),
    >,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    for (entity, mut rng, id, name, data_error) in query.iter_mut() {
        if data_error.attribute != "firstName" {
            unimplemented!() // Note: currently only data errors in first names supported
        }

        if !DataError::event_correction_occurs(&mut rng, &clock_state) {
            continue;
        }

        // Else: set the correct first name for the person
        let mut new_name = name.clone();
        new_name.first_name = data_error.intended_value.clone();

        debug!(
            time = clock_state.get_time().to_string(),
            eid = entity.index(),
            first_name = new_name.first_name,
            prefix = new_name.prefix,
            family_name = new_name.family_name,
            "NAME    "
        );

        let entity_cmd = commands.get_entity(entity);

        if let Some(mut entity_cmd) = entity_cmd {
            let message = name_change_event(
                &mut rng,
                &clock_state.get_time(),
                id.uuid,
                &new_name.first_name,
                &new_name.prefix,
                &new_name.family_name,
            );

            // Insert the new name bundle (which will override the existing one) and remove the data error bundle
            entity_cmd.insert(new_name).remove::<DataError>();

            nats_state
                .connection
                .publish(NAME_TOPIC, message.to_string())
                .unwrap();
        }
    }
}
