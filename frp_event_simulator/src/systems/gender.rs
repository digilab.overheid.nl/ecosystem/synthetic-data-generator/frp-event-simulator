use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    data::names::{FIRST_NAMES_FEMALE, FIRST_NAMES_MALE, FIRST_NAMES_X},
    event::{gender_change_event, name_change_event, GENDER_TOPIC, NAME_TOPIC},
};

#[allow(clippy::type_complexity)]
pub fn sim_gender_change(
    mut commands: Commands,
    mut query: Query<
        (Entity, &mut RngComponent, &Id, &Gender, &PersonName),
        (With<Person>, With<Gender>),
    >,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    for (entity, mut rng, id, gender, name) in query.iter_mut() {
        if !Gender::event_gender_change_occurs(&mut rng, &clock_state) {
            continue;
        }

        // Else...
        let now = clock_state.get_time();

        // Change gender to one of the other genders. Note: currently only from F to M or from M to F, see comment in gender.rs
        let new_gender_value = match gender.0 {
            GenderValue::Female => GenderValue::Male,
            GenderValue::Male => GenderValue::Female,
            GenderValue::X => continue, // Gender change for X currently not simulated, see comment above
        };

        // If the person's first name does not exist in the names for this gender or the names for gender X, randomly choose a new name that corresponds to the gender. Note: this can be done simultaneously with the gender change according to https://www.rijksoverheid.nl/binaries/rijksoverheid/documenten/brochures/2014/06/20/informatieblad-wet-wijziging-vermelding-van-het-geslacht-in-de-geboorteakte-transgenders/Informatieblad+Wet+wijziging+vermelding+van+het+geslacht+in+de+geboorteakte+%28transgenders%29+14+april+2023.pdf
        let keep_first_name = match new_gender_value {
            GenderValue::Female => {
                FIRST_NAMES_FEMALE.iter().any(|&s| s == name.first_name)
                    || FIRST_NAMES_X.iter().any(|&s| s == name.first_name)
            }
            GenderValue::Male => {
                FIRST_NAMES_MALE.iter().any(|&s| s == name.first_name)
                    || FIRST_NAMES_X.iter().any(|&s| s == name.first_name)
            }
            GenderValue::X => unreachable!(), // See comment above
        };

        debug!(
            time = now.to_string(),
            f_id = entity.index(),
            gender = new_gender_value.to_string(),
            "GENDER  "
        );

        let entity_cmd = commands.get_entity(entity);

        if let Some(mut entity_cmd) = entity_cmd {
            // Insert the new gender bundle (which will override the existing one)
            entity_cmd.insert(Gender(new_gender_value));

            let message = gender_change_event(
                &mut rng,
                &clock_state.get_time(),
                id.uuid,
                &new_gender_value,
            );

            nats_state
                .connection
                .publish(GENDER_TOPIC, message.to_string())
                .unwrap();

            if !keep_first_name {
                let mut new_name = name.clone();
                new_name.first_name = random_first_name(&new_gender_value, &mut rng);

                debug!(
                    time = clock_state.get_time().to_string(),
                    eid = entity.index(),
                    first_name = new_name.first_name,
                    prefix = new_name.prefix,
                    family_name = new_name.family_name,
                    "NAME    "
                );

                let message = name_change_event(
                    &mut rng,
                    &clock_state.get_time(),
                    id.uuid,
                    &new_name.first_name,
                    &new_name.prefix,
                    &new_name.family_name,
                );

                // Insert the new name bundle (which will override the existing one)
                entity_cmd.insert(new_name);

                nats_state
                    .connection
                    .publish(NAME_TOPIC, message.to_string())
                    .unwrap();
            }
        }
    }
}
