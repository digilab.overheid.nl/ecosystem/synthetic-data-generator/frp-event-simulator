use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};
use std::fmt;

// Define an enum representing different types/values of genders
#[derive(Copy, Clone, Default)]
pub enum GenderValue {
    Female,
    Male,
    #[default]
    X,
}

#[derive(Component)]
pub struct Gender (pub GenderValue);

// Implement the Display trait for the GenderValue enum
impl fmt::Display for GenderValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            GenderValue::Female => write!(f, "F"),
            GenderValue::Male => write!(f, "M"),
            GenderValue::X => write!(f, "X"),
        }
    }
}

impl GenderValue {
    pub fn from_str(s: &str) -> Option<Self> {
        match s {
            "F" => Some(GenderValue::Female),
            "M" => Some(GenderValue::Male),
            "X" => Some(GenderValue::X),
            _ => None,
        }
    }
}

impl Gender {
    // According to https://www.cbs.nl/en-gb/visualisations/dashboard-population/men-and-women, the male-female ratio at an age of 0-5 years in The Netherlands is approximately 51.2% - 48.8%
    pub fn event_female_at_birth_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(48.8)
            .scale(Duration::hours(24), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }

    // According to https://www.cbs.nl/-/media/_excel/2022/27/transseksuelen-2021.xlsx, per year ~700 people out of a population of ~17.5 million people change gender from M to F or vice versa, i.e. ~0.004% per year. According to https://www.rijksoverheid.nl/binaries/rijksoverheid/documenten/rapporten/2023/07/07/tk-bijlage-1-onderzoeksrapport-rutgers-kiezen-voor-een-x/tk-bijlage-1-onderzoeksrapport-rutgers-kiezen-voor-een-x.pdf and https://transvisie.nl/algemene-informatie/x-op-het-paspoort/, changing the gender to X is currently only possible via a court case and happens rarely, so this is currently not modelled yet. IMPROVE: add to the simulation, especially when implemented in the Dutch law
    pub fn event_gender_change_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(0.004)
            .scale(Duration::days(365), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}
