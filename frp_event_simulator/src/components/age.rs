use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::{Duration, NaiveDateTime};
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};
use statrs::distribution::{Continuous, Normal};

const DAYS_PER_YEAR: f64 = 365.0;

#[derive(Component)]
pub struct Age {
    pub birth_date: NaiveDateTime,
}

impl Age {
    pub fn new(birth_date: NaiveDateTime) -> Self {
        Self { birth_date }
    }

    pub fn age(&self, now: NaiveDateTime) -> Duration {
        now.signed_duration_since(self.birth_date)
    }

    pub fn event_birth_registration_occurs(
        &self,
        rng: &mut RngComponent,
        clock_state: &ClockState,
    ) -> bool {
        // According to https://www.rijksoverheid.nl/onderwerpen/aangifte-geboorte-en-naamskeuze-kind/vraag-en-antwoord/aangifte-geboorte, birth registration (geboorteaangifte) should occur within 3 full days after the birth of a child, with a minimum of 2 working days
        // For simplicity, we assume the formula P(a) = 0.2 * a, with a maximum of 1, where `a` is the age of the child in days
        // IMPROVE: take business days into account
        let now: NaiveDateTime = clock_state.get_time();
        let age_in_days = self.age(now).num_days() as f64;

        // When the age is negative, return false
        if age_in_days < 0.0 {
            return false;
        }

        let rand = rng.u64(0..u64::MAX);

        let daily_birth_registration_chance: f64 = (0.2 * age_in_days).min(1.0);

        let chance = PercentageChance::new(daily_birth_registration_chance * 100.0)
            .scale(Duration::hours(24), clock_state.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }

    pub fn event_death_occurs(&self, rng: &mut RngComponent, clock_state: &ClockState) -> bool {
        // The probability of death is based on this dataset:
        // https://opendata.cbs.nl/statline/#/CBS/nl/dataset/37360ned/table?fromstatweb
        // We take the 2022 data and use the average of male and female subjects. Then, since people in general do not reach a higher age than 120 years (https://en.wikipedia.org/wiki/List_of_the_verified_oldest_people), we add an extra data point of a probability of death of 1 at 120 years. This results in the following data points for the probability of death within a year, given the person's age in years:
        // 0    0.00445
        // 21   0.00032
        // 61   0.005845
        // 81   0.048975
        // 120  1
        // With curve fitting (e.g. using a tool like https://mycurvefit.com/), assuming a power curve, we obtain the following equation:
        // y = 1.119401e-16 * x^7.671776
        // IMPROVE: compensate for COVID-19 deaths, see https://www.cbs.nl/nl-nl/nieuws/2024/06/sterfte-in-2023-afgenomen for some details
        // Note: an alternative would be to define a random life span per agent at birth and at each step to check whether or not the life span is reached. However, little information seems to be available about the life span standard deviation, except that is was 'approximately 1 year' around 2008

        let now = clock_state.get_time();
        let age = self.age(now).num_days() as f64 / DAYS_PER_YEAR;

        // When the age is negative, return false
        if age < 0.0 {
            return false;
        }

        let daily_death_chance = 1.119401e-16_f64 * age.powf(7.671776) / DAYS_PER_YEAR;

        let chance = PercentageChance::new(daily_death_chance * 100.0)
            .scale(Duration::hours(24), clock_state.time_per_tick)
            .fraction_of_u64();

        let rand = rng.u64(0..u64::MAX);

        rand < chance
    }

    pub fn event_pregnancy_occurs(&self, rng: &mut RngComponent, clock_state: &ClockState) -> bool {
        let now = clock_state.get_time();

        let rand = rng.u64(0..u64::MAX);

        let chance = PercentageChance::new(daily_preg_chance(self.age(now)) * 100.0)
            .scale(Duration::hours(24), clock_state.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}

fn daily_preg_chance(age: Duration) -> f64 {
    // Probability of birth
    // Source: https://opendata.cbs.nl/#/CBS/nl/dataset/37201/table?dl=94E25 and https://opendata.cbs.nl/#/CBS/nl/dataset/03759ned/table?dl=94E28
    // We take the probability, given the age of a female* person in years, that this person will bear a living child in one year time
    // Interpolated with Gaussian Bell curve fit (e.g. using https://mycurvefit.com/)
    // This results in the following data points:
    // 12.5     0.00020557
    // 22.5     0.00858504
    // 27.5     0.03612038
    // 32.5     0.05931208
    // 37.5     0.03316359
    // 42.5     0.00722938
    // 60       0.00219446
    // Resulting parameters:
    // mean = 32.29439, standard deviation = 4.904455, factor 'a' = 0.05897603

    let age = age.num_days() as f64 / DAYS_PER_YEAR;

    // When the age is negative, return a probability of 0
    if age < 0_f64 {
        return 0.0;
    }

    let normal = Normal::new(32.29439, 4.904455).unwrap();

    // When using a Normal distribution, the factor 'a' is 1 / (standard_devication * sqrt(2*pi)) = approximately 0.08134283633. However in our case, 'a' = 0.05897603, so we have to compensate for this
    0.05897603 / 0.08134283633 * normal.pdf(age) / 2.0 / DAYS_PER_YEAR // Note: divided by two, since we assume that half of the population is female*
}

// *: Here 'female' denotes someone who is, when having a child, indicated as 'mother' by the CBS
