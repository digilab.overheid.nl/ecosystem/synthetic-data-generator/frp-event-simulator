use bevy::prelude::*;
use std::fmt;
// use bevy_turborand::{DelegatedRng, RngComponent};
// use chrono::Duration;
// use sdg_event_simulator_common::{
//     bevy::resources::clock_state::ClockState, chance::PercentageChance,
// };
use uuid::Uuid;

// Address is a subset of the full addresses as generated by the seeder and 'adressen en gebouwen' simulator
#[derive(Component, Clone, Debug, Default)]
pub struct Address {
    pub id: Uuid,
    pub municipality: (String, String),
    pub purpose: String,
}

impl Address {
    pub fn new(id: Uuid, municipality: (String, String), purpose: String) -> Self {
        Self {
            id,
            municipality,
            purpose,
        }
    }

    // // event_occurs is used to define when the event occurs during lifetime. Note that the address is also set initially for a person at birth registration (geboorteaangifte)
    // pub fn event_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
    //     let rand = rng.u64(0..u64::MAX);
    //     let chance = PercentageChance::new(10.0)
    //         .scale(Duration::hours(24), time.time_per_tick)
    //         .fraction_of_u64();

    //     rand < chance
    // }
}

// AddressReference contains only a reference (UUID) to an address entity
#[derive(Component, Clone, Debug, Default)]
pub struct AddressReference(pub Uuid);

impl fmt::Display for AddressReference {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0.to_string())
    }
}
