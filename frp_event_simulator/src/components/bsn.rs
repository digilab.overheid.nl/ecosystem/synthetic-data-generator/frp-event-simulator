use bevy::prelude::*;

#[derive(Component, Default, Clone, Debug)]
pub struct BSN(pub u32);
