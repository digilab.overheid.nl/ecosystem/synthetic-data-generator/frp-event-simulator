use std::{env, process::exit};

use bevy::prelude::*;
use tracing_subscriber::{filter::LevelFilter, EnvFilter};

use sdg_event_simulator_common::{
    bevy::{
        plugins::SdgPluginGroup, resources::simulator_identity::SimulatorIdentity,
        run_loop::run_loop,
    },
    flags::{setup_signals, wait_for_exit},
};

mod components;
mod data;
mod event;
mod resources;
mod systems;

use resources::{AddressState, BsnState, SeedState};
use systems::{init_systems, update_systems};

fn main() {
    let subscriber = tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .compact()
        .finish();
    tracing::subscriber::set_global_default(subscriber).unwrap();

    info!("Starting up simulator");

    setup_signals();

    App::new()
        .set_runner(run_loop)
        .add_plugins(SdgPluginGroup)
        .insert_resource(SimulatorIdentity::new("frp-event-simulator"))
        .insert_resource(SeedState::default())
        .insert_resource(BsnState::default())
        .insert_resource(AddressState::default())
        .add_systems(PreStartup, init_systems::load_seed_births)
        .add_systems(Startup, init_systems::load_seed_addresses)
        .add_systems(PostStartup, init_systems::load_seed_deaths)
        .add_systems(PostStartup, init_systems::inspect_seeded)
        .add_systems(Update, update_systems::sim_pregnancy)
        .add_systems(Update, update_systems::sim_birth)
        .add_systems(Update, update_systems::sim_birth_registration)
        .add_systems(Update, update_systems::sim_name_change)
        .add_systems(Update, update_systems::sim_gender_change)
        // .add_systems(Update, update_systems::sim_address_registration)
        .add_systems(Last, update_systems::sim_death)
        .add_systems(Update, update_systems::sim_data_error_correction)
        .run();

    let sleep_on_end = env::var("SLEEP").map(|v| v != "n").unwrap_or_else(|_| true);
    if sleep_on_end {
        wait_for_exit();
    }

    exit(0)
}
